﻿using UnityEngine;
using System.Collections.Generic;
using System;

[Serializable]
public class HighscoreTable
{
	public const int CAPACITY = 10;
	private const int INVALID_INDEX = -1;

	[SerializeField]
	private List<Highscore> _scores;

	public List<Highscore> Scores { get { return _scores; } }

	public HighscoreTable()
	{
		_scores = new List<Highscore>();
	}

	private int FindPositionOfScoreInList(Highscore newScore)
	{
		var i = 0;
		while (i < _scores.Count)
		{
			if (_scores[i].score < newScore.score)
			{
				return i;
			}
			i++;
		}
		return i < CAPACITY ? i : INVALID_INDEX;
	}

	public bool AddHighscore(Highscore newScore)
	{
		var newIndex = FindPositionOfScoreInList(newScore);
		var isHighscore = newIndex != INVALID_INDEX;
		if (isHighscore)
		{
			_scores.Insert(newIndex, newScore);
			if (_scores.Count > CAPACITY)
			{
				_scores.RemoveRange(CAPACITY, _scores.Count - CAPACITY);
				Debug.Assert(_scores.Count == CAPACITY, "log to make sure capacity is preserved");
			}
		}

		return isHighscore;
	}
}