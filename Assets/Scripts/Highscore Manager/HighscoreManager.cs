﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class HighscoreManager : MonoBehaviour
{
	public static HighscoreManager Instance { get; private set; }
	public HighscoreTable HighscoreTable;
	private const string playerPrefKey = "highscoreTable";
	void Start()
	{
		Instance = this;
		Read();
		GameController.Instance.OnPlayerDead.AddListener((sender) =>
		{
			var playerData = GameController.Instance.PlayerData;
			HighscoreTable.AddHighscore(new Highscore() { score = playerData.Score, date = DateTime.Now.ToShortDateString() });
			Write();
		});
	}

	void Read()
	{
		var str = PlayerPrefs.GetString(playerPrefKey, string.Empty);
		HighscoreTable = JsonUtility.FromJson<HighscoreTable>(str);
		if (HighscoreTable == null)
		{
			HighscoreTable = new HighscoreTable();
		}
		Debug.Log("highscore list is read from player prefs. Count: " + HighscoreTable.Scores.Count);
	}

	void Write()
	{
		var str = JsonUtility.ToJson(HighscoreTable);
		Debug.Log("write " + str);
		PlayerPrefs.SetString(playerPrefKey, str);
	}
}
