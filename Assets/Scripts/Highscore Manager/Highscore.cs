﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class Highscore
{
	public int score;
	public string date;
}
