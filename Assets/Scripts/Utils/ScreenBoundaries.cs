﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Automatically generates a BoxCollider2D to cover the screen,
/// updates the collider when screen is resized.
/// </summary>
public class ScreenBoundaries : MonoBehaviour
{

	private Camera cam;
	/// <summary>
	/// In counter-clockwise direction, starts from bottom-left corner.
	/// </summary>
	private Vector3[] _viewportPoints;
	/// <summary>
	/// Corresponding world coordinates to _viewportPoints
	/// </summary>
	private static Vector3[] _worldPoints;

	public static Rect Rect
	{
		get
		{
			var a = new Rect();
			a.min = _worldPoints[0];
			a.max = _worldPoints[2];
			return a;
		}
	}

	public static Rect SafeRect
	{
		get
		{
			var a = new Rect();
			var dif = Rect.max - Rect.min;
			a.min = Rect.min - dif;
			a.max = Rect.max + dif;
			return a;
		}
	}

	private BoxCollider2D _boxCollider;
	void Awake()
	{
		CalculateWorldPoints();
		CreateCollider();
	}

	void CalculateWorldPoints()
	{
		cam = Camera.main;
		var dist = -cam.transform.position.z;
		_viewportPoints = new Vector3[]
		{
			new Vector3(0f, 0f, dist),
			new Vector3(1f, 0f, dist),
			new Vector3(1f, 1f, dist),
			new Vector3(0f, 1f, dist),
		};
		_worldPoints = new Vector3[_viewportPoints.Length];
		var i = 0;
		while (i < _viewportPoints.Length)
		{
			_worldPoints[i] = cam.ViewportToWorldPoint(_viewportPoints[i]);
			i++;
		}
	}

	void CreateCollider()
	{
		if (_boxCollider == null)
		{
			_boxCollider = gameObject.AddComponent<BoxCollider2D>();
			_boxCollider.isTrigger = true;
		}
		var i = 0;
		Bounds bounds = new Bounds();
		while (i < _worldPoints.Length)
		{
			bounds.Encapsulate(_worldPoints[i]);
			//GameObject.CreatePrimitive(PrimitiveType.Cube).transform.position = _worldPoints[i];
			//Debug.Log("encapsulate " + _worldPoints[i] + " min: " + bounds.min + " max: " + bounds.max);
			i++;
		}
		_boxCollider.size = bounds.size;
		_boxCollider.offset = cam.transform.position;
	}

	void OnDrawGizmos()
	{
		if (Application.isPlaying)
		{
			Gizmos.DrawWireCube(SafeRect.center, SafeRect.size);
		}
	}
}