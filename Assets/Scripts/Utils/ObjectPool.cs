﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class ObjectPool<T>
{
	private Queue<T> _objects;
	private Func<T> _objectGenerator;

	public ObjectPool(Func<T> objectGenerator)
	{
		Debug.Assert(objectGenerator != null, "objectGenerator");
		_objects = new Queue<T>();
		_objectGenerator = objectGenerator;
	}

	public T GetObject()
	{
		if (_objects.Count > 0) return _objects.Dequeue();
		return _objectGenerator();
	}

	public void ReturnObject(T item)
	{
		//Debug.Assert(!_objects.Contains(item));
		if (_objects.Contains(item))
		{
			_objects.Enqueue(item);
		}
	}
}