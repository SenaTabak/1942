﻿using UnityEngine;
using System.Collections;

public static class LayerMaskExtension
{
	public static bool IsInLayerMask(this LayerMask mask, int layer)
	{
		return ((mask.value & (1 << layer)) > 0);
	}
	
}
