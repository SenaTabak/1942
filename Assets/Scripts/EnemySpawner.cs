﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Wave
{
	public List<string> row;
}

[System.Serializable]
public class Level
{
	public List<Wave> waves;
}

public class EnemySpawner : MonoBehaviour
{
	public Level level;
	public int RowCount;
	public float Frequency;

	private Transform _shipContainer;
	void Start()
	{
		GameController.Instance.OnPlay.AddListener(() =>
		{
			Reset();
		});
	}

	void Initialize()
	{
		InvokeRepeating("Spawner", 0f, 1f / Frequency);
	}

	void Reset()
	{
		CancelInvoke("Spawner");
		currentWave = 0;
		Initialize();
	}

	private int currentWave;
	void Spawner()
	{
		var wave = level.waves[currentWave];
		currentWave++;
		currentWave = currentWave >= level.waves.Count ? 0 : currentWave;

		var number = wave.row.Count;
		var i = 0;
		while (i < number)
		{
			var type = wave.row[i];
			i++;
			if (type == "-")
			{
				continue;
			}
			var spaceship = GameController.Instance.SGFactory.GetSGObject(new SGType() { Type = type});
			spaceship.transform.rotation = Quaternion.identity;
			spaceship.transform.Rotate(Vector3.back, 180f);
			var columnWidth = ScreenBoundaries.Rect.size.x / RowCount;
			var pos = new Vector3(ScreenBoundaries.Rect.min.x + (i - 1) * columnWidth + columnWidth * 0.5f,
				ScreenBoundaries.Rect.max.y * 1.2f, 0f);

			spaceship.transform.position = pos;

#if UNITY_EDITOR // avoid hierarchy to get crowded in editor, by grouping
			if (_shipContainer == null)
			{
				_shipContainer = new GameObject("_shipContainer").transform;
			}
			spaceship.transform.SetParent(_shipContainer.transform);
#endif
		}
	}
}
