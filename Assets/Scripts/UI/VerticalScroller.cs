﻿using UnityEngine;
using System.Collections;

public class VerticalScroller : MonoBehaviour
{
	public Vector3 Velocity = Vector3.up;

	private Vector3 screenSize;
	private Vector3 shift;
	private Vector3 initialPos;
	void Start()
	{
		var size = GetComponent<SpriteRenderer>().bounds.size;
		screenSize = ScreenBoundaries.Rect.size;

		transform.localScale = new Vector3(screenSize.x / size.x, 3 * screenSize.y / size.y);
		transform.position = new Vector3(0f, ScreenBoundaries.Rect.min.y + screenSize.y * 1.5f, 0f);

		initialPos = transform.position;
	}

	void FixedUpdate()
	{
		shift += Velocity * Time.deltaTime;
		transform.position = initialPos + shift;

		if (Mathf.Abs(shift.y) > screenSize.y * 2)
		{
			transform.position = initialPos;
			shift = Vector3.zero;
		}
	}
}
