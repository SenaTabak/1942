﻿using UnityEngine;
using System.Collections;

public class PauseMenuBehaviour : MonoBehaviour
{

	void OnEnable()
	{
		//Debug.Log("Pause");
		Time.timeScale = 0;
	}

	void OnDisable()
	{
		//Debug.Log("Resume");
		Time.timeScale = 1;
	}
}
