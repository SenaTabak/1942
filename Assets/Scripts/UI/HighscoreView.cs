﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HighscoreView : MonoBehaviour
{
	public Text HighscoreText;

	void OnEnable()
	{
		var scores = HighscoreManager.Instance.HighscoreTable.Scores;
		var formattedScoreStr = "";
		foreach (var score in scores)
		{
			formattedScoreStr += score.score + "   " + score.date + "\n";
		}
		HighscoreText.text = formattedScoreStr;
	}

}
