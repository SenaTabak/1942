﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerView : MonoBehaviour
{
	public Text HealthText;
	public Text ScoreText;
	private PlayerData PlayerData;

	void Update()
	{
		var PlayerData = GameController.Instance.Player.gameObject.GetComponent<PlayerData>();
		HealthText.text = PlayerData.GetComponent<Hull>().GetHealth().ToString();
		ScoreText.text = PlayerData.Score.ToString();
	}
}
