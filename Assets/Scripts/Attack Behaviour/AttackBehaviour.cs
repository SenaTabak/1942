﻿using UnityEngine;
using System.Collections;

public abstract class AttackBehaviour : MonoBehaviour
{
	[SerializeField]
	private SGType GunType;

	public void SetGunType(SGType gunType)
	{
		if (gunType != GunType)
		{
			CreateGun(gunType);
		}
	}

	protected Gun _gun;

	protected virtual void OnEnable()
	{
		CreateGun(GunType);
	}

	protected virtual void OnDisable()
	{
		GameController.Instance.SGFactory.ReturnSGObject(_gun);
		_gun = null;
	}

	void CreateGun(SGType gunType)
	{
		_gun = GameController.Instance.SGFactory.GetSGObject(gunType).GetComponent<Gun>();
		_gun.transform.SetParent(transform, false);
		_gun.gameObject.layer = gameObject.layer;
	}

	protected void Fire()
	{
		Debug.Assert(gameObject.activeInHierarchy, "not active");
		_gun.Fire();
	}

}
