﻿using UnityEngine;
using System.Collections;

public class EnemyAttackBehaviour : AttackBehaviour
{
	public float FireFrequencyInSeconds;

	void OnDisable()
	{
		CancelInvoke("Fire");
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		InvokeRepeating("Fire", 0f, 1f / FireFrequencyInSeconds);
	}
}
