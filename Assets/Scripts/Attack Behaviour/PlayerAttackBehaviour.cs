﻿using UnityEngine;
using System.Collections;

public class PlayerAttackBehaviour : AttackBehaviour
{
	public float longPressFireDelay = 0.25f;

	void Update()
	{
		if (Time.timeScale == 0)
		{
			return;
		}
		if (Input.GetButtonDown("Fire1"))
		{
			InvokeRepeating("Fire", 0f, longPressFireDelay);
		}

		if (Input.GetButtonUp("Fire1"))
		{
			CancelInvoke("Fire");
		}
	}

	protected override void OnDisable()
	{
		base.OnDisable();
		CancelInvoke("Fire");
	}

}
