﻿using UnityEngine;
using System.Collections;

public class CollisionDamage : MonoBehaviour
{
	public int Damage;

	public void OnTriggerEnter2D(Collider2D collision)
	{
		var hull = collision.gameObject.GetComponent<Hull>();
		if (hull != null)
		{
			//Debug.Log("damage " + hull.name);
			hull.DecreaseHealth(Damage);
		}
	}

}
