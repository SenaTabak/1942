﻿using UnityEngine;
using System.Collections;

public class ParticleDestroy : MonoBehaviour
{
	private ParticleSystem _system;

	void Start()
	{
		_system = GetComponent<ParticleSystem>();
	}

	private bool isStarted = false;
	void Update()
	{
		if (_system.particleCount > 0)
		{
			isStarted = true;
		}
		if (isStarted && _system.particleCount == 0)
		{
			Destroy(gameObject);
		}
	}
}
