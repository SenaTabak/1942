﻿using UnityEngine;
using System.Collections;

public class DropItem : SGObject
{
	public SGType DropType { get; set; }
	public LayerMask LayerMask;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (LayerMask.IsInLayerMask(collider.gameObject.layer))
		{
			var attackBehaviour = collider.GetComponent<AttackBehaviour>();
			//Debug.LogWarning("drop type: " + DropType);
			attackBehaviour.SetGunType(DropType);
			GameController.Instance.SGFactory.ReturnSGObject(this);
		}
	}
}
