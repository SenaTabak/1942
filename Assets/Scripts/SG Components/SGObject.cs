﻿using UnityEngine;
using System.Collections;

/// <summary>
/// This is used in pooling and also object factory. It is exposed to inspector to allow defining types in editor time. This makes it possible to avoid defining game specific types in the script.
/// </summary>
[System.Serializable]
public class SGType
{
	public string Type = "Default";
}

public class SGObject : MonoBehaviour // stands for SpaceGameObject
{
	public SGType Type;

	[HideInInspector]
	public bool HasPool;

	void Start()
	{
		if (HasPool)
		{
			GameController.Instance.OnPlay.AddListener(() =>
			{
				GameController.Instance.SGFactory.ReturnSGObject(this);
			});
		}
	}
}
