﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomDropItemSpawner : MonoBehaviour
{
	[Range(0f, 1f)]
	public float DropProbability;

	public SGType DropItemType;
	void OnDisable()
	{
		var isDead = GetComponent<Hull>().GetHealth() == 0;
		//Debug.Log("on disable " + isDead);
		if (isDead)
		{
			SpawnDropItem(GameController.Instance.Assets.PlayerGuns);
		}
	}

	void SpawnDropItem<T>(List<T> list) where T : SGObject
	{
		var willDrop = DropProbability >= Random.value;
		if (!willDrop)
		{
			return;
		}
		var dropItem = GameController.Instance.SGFactory.GetSGObject(DropItemType).GetComponent<DropItem>();

		var randomIndex = Random.Range(0, list.Count);
		var randomDropType = list[randomIndex].Type;
		dropItem.DropType = randomDropType;
		dropItem.transform.position = transform.position;
	}
}
