﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Events;
using System.Collections.Generic;

public class Hull : MonoBehaviour
{
	[FormerlySerializedAs("Health")]
	[SerializeField]
	private int InitialHealth;
	
	[FormerlySerializedAs("HullDamage")]
	public ParticleSystem[] HullDamageAssets;

	private int _health;

	public int GetHealth()
	{
		return _health;
	}

	public int GetInitialHealth()
	{
		return InitialHealth;
	}

	void OnEnable()
	{
		_health = InitialHealth;
	}

	public void DecreaseHealth(int amount)
	{
		_health = Mathf.Max(0, _health - amount);
		//Debug.Log("new health: " + _health + " amount " + amount);
		OnHealthUpdated();
		//return;
		CreateAnimation();
	}

	private void CreateAnimation()
	{
		if (HullDamageAssets.Length > 0)
		{
			var randomHullDamage = Random.Range(0, HullDamageAssets.Length);
			//Debug.Log("randomHullDamage prefab: " + randomHullDamage);
			var bounds = GetComponent<BoxCollider2D>().bounds;
			var min = bounds.min;
			var max = bounds.max;
			var randomPos = new Vector3(Random.Range(min.x, max.x), Random.Range(min.y, max.y), Random.Range(min.z, max.z));
			Instantiate(HullDamageAssets[randomHullDamage], randomPos, Quaternion.identity);
		}
	}

	//public void Update()
	//{
	//	var i = 0;
	//	while (i < _particles.Count)
	//	{
	//		var isComplete = _particles[i].is
	//		//Debug.Log("time " + time);
	//		if (isComplete)
	//		{
	//			Destroy(_particles[i].gameObject);
	//			_particles.RemoveAt(i);
	//			i--;
	//		}
	//		i++;
	//	}
	//}

	void OnHealthUpdated()
	{
		if (_health <= 0)
		{
			var sgObject = gameObject.GetComponent<SGObject>();
			// Player dead
			if (gameObject.layer == LayerMask.NameToLayer(GameController.LAYER_PLAYER))
			{
				//Debug.Log("on player dead");
				GameController.Instance.OnPlayerDead.Invoke(gameObject);
				//GameController.Instance.Player.gameObject.SetActive(false);
			}
			// Enemy dead
			else if (gameObject.layer == LayerMask.NameToLayer(GameController.LAYER_ENEMY))
			{
				//Debug.Log("on enemy dead");
				GameController.Instance.OnSpaceshipDead.Invoke(gameObject);
				GameController.Instance.SGFactory.ReturnSGObject(sgObject);
			}
			// Bullet dead
			else if (gameObject.layer == LayerMask.NameToLayer(GameController.LAYER_ENEMY + GameController.LAYER_BULLET) ||
				gameObject.layer == LayerMask.NameToLayer(GameController.LAYER_PLAYER + GameController.LAYER_BULLET))
			{
				//Debug.Log("on bullet dead");
				GameController.Instance.SGFactory.ReturnSGObject(sgObject);
			}
		}
	}
}

