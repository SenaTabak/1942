﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;

/// <summary>
/// Data class used to pass configuration to each single bullet in a gun.
/// </summary>
[System.Serializable]
public class BulletConfig
{
	public Vector3 Position;
	public Vector2 Direction;
	public float Speed;
	public SGObject Bullet;
}

public class Gun : SGObject
{

	#region Bullets

	public BulletConfig[] Bullets;
	/// <summary>
	/// Bullets will be created as a child object of the container, to avoid scene hierarchy to get too crowded.
	/// </summary>
	private static Transform _bulletContainer;
	public SGObject GetBullet(SGType bulletType)
	{
		var bullet = GameController.Instance.SGFactory.GetSGObject(bulletType);

#if UNITY_EDITOR // avoid hierarchy to get crowded in editor, by grouping
		if (_bulletContainer == null)
		{
			_bulletContainer = new GameObject("_bulletContainer").transform;
		}
		bullet.transform.SetParent(_bulletContainer.transform);
#endif
		//bullet.gameObject.SetActive(true);
		return bullet;
	}
	#endregion

	public void Fire()
	{
		foreach (var bulletConfig in Bullets)
		{
			var pos = transform.position + transform.rotation * bulletConfig.Position;
			var alpha = Mathf.Rad2Deg * Mathf.Atan2(bulletConfig.Direction.y, bulletConfig.Direction.x);
			var rot = transform.rotation * Quaternion.AngleAxis(alpha - 90f, Vector3.forward);
			var bullet = GetBullet(bulletConfig.Bullet.Type);
			bullet.GetComponent<LinearSteeringBehaviour>().Speed = bulletConfig.Speed;
			bullet.transform.position = pos;
			bullet.transform.rotation = rot;
			bullet.gameObject.layer = LayerMask.NameToLayer(LayerMask.LayerToName(gameObject.layer) + GameController.LAYER_BULLET);
			var dir = transform.rotation * bulletConfig.Direction;
			bullet.GetComponent<LinearSteeringBehaviour>().Dir = dir;
		}
	}

	void OnDrawGizmos()
	{
		if (Bullets == null)
		{
			return;
		}
		var i = 0;
		while (i < Bullets.Length)
		{
			Debug.DrawRay(Bullets[i].Position, Bullets[i].Direction, Color.red);
			i++;
		}
	}

}
