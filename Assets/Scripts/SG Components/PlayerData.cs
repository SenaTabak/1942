﻿using UnityEngine;
using System.Collections;

public class PlayerData : MonoBehaviour
{
	public int Score { get; set; }
	public int Distance { get; set; }
	public int EnemyKilled { get; set; }

	void OnEnable()
	{
		Score = 0;
		Distance = 0;
		EnemyKilled = 0;
	}

}
