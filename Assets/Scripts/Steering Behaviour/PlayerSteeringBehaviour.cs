﻿using UnityEngine;
using System.Collections;

public class PlayerSteeringBehaviour : SteeringBehaviour
{

	protected override void FixedUpdate()
	{
		var diffVector = GetDiffVector();
		var newPos = transform.position + diffVector;
		var allowedRect = ScreenBoundaries.Rect;
		newPos.x = Mathf.Clamp(newPos.x, allowedRect.min.x, allowedRect.max.x);
		newPos.y = Mathf.Clamp(newPos.y, allowedRect.min.y, allowedRect.max.y);
		transform.position = newPos;
	}

	private Vector3 GetDiffVector()
	{
		if (Input.touchCount > 0)
		{
			var touch = Input.touches[0].deltaPosition;
			return new Vector3(touch.x / Screen.width, touch.y / Screen.height, 0f) * Speed * 100;
		}
		else
		{

			return new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0f) * Speed;
		}
	}
}
