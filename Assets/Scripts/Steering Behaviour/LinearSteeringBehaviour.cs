﻿using UnityEngine;
using System.Collections;

public class LinearSteeringBehaviour : SteeringBehaviour
{
	protected override void FixedUpdate()
	{
		Dir = gameObject.transform.up;
		base.FixedUpdate();
	}
}
