﻿using UnityEngine;
using System.Collections;

public class SteeringBehaviour : MonoBehaviour
{
	public Vector2 Dir = Vector2.up;
	public float Speed = 1f;

	protected virtual void FixedUpdate()
	{
		var diff = Dir * Speed * Time.fixedDeltaTime;
		transform.position += new Vector3(diff.x, diff.y, 0f);

		var rect = gameObject.layer == LayerMask.NameToLayer(GameController.LAYER_PLAYER_BULLET) ||
			gameObject.layer == LayerMask.NameToLayer(GameController.LAYER_ENEMY_BULLET) ? ScreenBoundaries.Rect : ScreenBoundaries.SafeRect;
		if (!rect.Contains(transform.position))
		{
			OutsideSafeRect();
		}
	}

	private void OutsideSafeRect()
	{
		var sgObject = gameObject.GetComponent<SGObject>();
		GameController.Instance.SGFactory.ReturnSGObject(sgObject);
	}

	void OnDrawGizmos()
	{
		Debug.DrawRay(transform.position, Dir * 2, Color.magenta);
	}
}