﻿using UnityEngine;
using System.Collections;

public class BossSteeringBehaviour : SteeringBehaviour
{
	private float PausePosHeight;

	void Start()
	{
		PausePosHeight = ScreenBoundaries.Rect.height * 3f / 4f + ScreenBoundaries.Rect.min.y;
	}

	protected override void FixedUpdate()
	{
		Dir = gameObject.transform.up;
		if (transform.position.y > PausePosHeight)
		{
			base.FixedUpdate();
		}
	}
}