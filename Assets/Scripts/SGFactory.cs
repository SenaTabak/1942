﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Factory class responsible of creating an object of desired type.
/// Internally, it uses object pools.
/// </summary>
public class SGFactory
{
	private Dictionary<string, ObjectPool<SGObject>> _pool;

	public SGFactory()
	{
		//Debug.Log("SG Factory Initialize");
		_pool = new Dictionary<string, ObjectPool<SGObject>>();

		CreatePoolFor(GameController.Instance.Assets.PlayerSpaceship);
		foreach (var spaceshipBlueprint in GameController.Instance.Assets.Spaceships)
		{
			CreatePoolFor(spaceshipBlueprint);
		}
		foreach (var gunBlueprint in GameController.Instance.Assets.Guns)
		{
			CreatePoolFor(gunBlueprint);
		}
		foreach (var gunBlueprint in GameController.Instance.Assets.PlayerGuns)
		{
			CreatePoolFor(gunBlueprint);
		}
		foreach (var bulletBlueprint in GameController.Instance.Assets.Bullets)
		{
			CreatePoolFor(bulletBlueprint);
		}
		foreach (var dropBlueprint in GameController.Instance.Assets.DropItems)
		{
			CreatePoolFor(dropBlueprint);
		}
	}

	private void CreatePoolFor(SGObject blueprint)
	{
		var type = blueprint.Type.Type;
		if (!_pool.ContainsKey(type))
		{
			_pool.Add(type, new ObjectPool<SGObject>(() =>
			{
				Debug.Assert(blueprint != null, "blueprint is null");
				var obj = Object.Instantiate(blueprint);
				Debug.Assert(obj != null, "Created obj is null");
				return obj;
			}));
		}
	}

	public SGObject GetSGObject(SGType type)
	{
		Debug.Assert(_pool.ContainsKey(type.Type), "Bad sg object type(" + type.Type + ")");
		var pool = _pool[type.Type];
		var obj = pool.GetObject();
		obj.HasPool = true;
		Debug.Assert(obj != null, "obj from pool is null. Type(" + type + ")");
		obj.gameObject.SetActive(true);
		Debug.Assert(obj.gameObject.activeSelf, "obj from pool is not active. Type(" + type + ")");
		return obj;
	}

	private static Transform _container;
	public void ReturnSGObject(SGObject obj)
	{
		//Debug.Log("return " + obj.name);
		var type = obj.Type.Type;
		Debug.Assert(obj != null, "returned obj is null. Type(" + type + ")");
		Debug.Assert(_pool.ContainsKey(type), "Bad sg object type(" + type + ")");
		var pool = _pool[type];
#if UNITY_EDITOR // avoid hierarchy to get crowded in editor, by grouping
		if (_container == null)
		{
			_container = new GameObject("Pooled SG Objects").transform;
		}
#endif
		//obj.gameObject.transform.SetParent(null);
		obj.gameObject.SetActive(false);
		pool.ReturnObject(obj);
	}

}
