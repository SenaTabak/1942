﻿using UnityEngine;
using System.Collections.Generic;

public class AssetDB : MonoBehaviour
{
	public SGType[] Types;
	public SGObject PlayerSpaceship;
	public List<SGObject> Spaceships;
	public List<Gun> Guns;
	public List<Gun> PlayerGuns;
	public List<SGObject> Bullets;
	public List<DropItem> DropItems;
}
