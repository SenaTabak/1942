﻿using SGEventSystem;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
	#region Constants
	public const string LAYER_PLAYER = "Player";
	public const string LAYER_ENEMY = "Enemy";
	public const string LAYER_BULLET = "Bullet";
	public const string LAYER_SCREEN = "Screen";
	public const string LAYER_PLAYER_BULLET = LAYER_PLAYER + LAYER_BULLET;
	public const string LAYER_ENEMY_BULLET = LAYER_ENEMY + LAYER_BULLET;
	#endregion
	public static GameController Instance;
	public AssetDB Assets;

	public SGObject Player { get; private set; }
	public PlayerData PlayerData { get; private set; }

	[HideInInspector]
	public UnityEvent OnPlay;
	[HideInInspector]
	public UnityEvent OnGameOver;

	public SGFactory SGFactory { get; private set; }

	void Awake()
	{
		Debug.Log("awake " + Instance);
		if (Instance != null)
		{
			Debug.Assert(false, "GameController should have only one instance!");
		}
		Instance = this;
		OnPlayerDead = new SGEvent(SGEventType.PlayerDead);
		OnSpaceshipDead = new SGEvent(SGEventType.SpaceshipDead);
		SGFactory = new SGFactory();
		OnSpaceshipDead.AddListener(SpaceshipDead);
	}

	public void PlayButtonDown()
	{
		OnPlay.Invoke();
		CreatePlayer();
	}

	public SGEvent OnSpaceshipDead;
	public SGEvent OnPlayerDead;

	private void CreatePlayer()
	{
		Debug.Log("CReate pl");
		Player = SGFactory.GetSGObject(Assets.PlayerSpaceship.Type).GetComponent<SGObject>();
		PlayerData = Player.GetComponent<PlayerData>();
	}

	private void SpaceshipDead(GameObject parameter)
	{
		var hull = parameter.GetComponent<Hull>();
		Debug.Assert(hull != null, "Should have Hull component");
		PlayerData.EnemyKilled += 1;
		PlayerData.Score += hull.GetInitialHealth();
	}

	public void PlayAgainButtonDown()
	{
		Debug.Assert(Instance != null, "GameController");
		PlayButtonDown();
	}

	public void Exit()
	{
		Application.Quit();
	}

}
