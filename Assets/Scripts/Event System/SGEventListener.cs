﻿using UnityEngine;
using UnityEngine.Events;

namespace SGEventSystem
{
	public class SGEventListener : MonoBehaviour
	{
		public SGEventType EventType;
		public UnityEvent OnEventFired;

		void Start()
		{
			var listenedEvent = SGEventManager.Instance.GetEventByType(EventType);
			//Debug.Log("listen " + listenedEvent.EventType);
			listenedEvent.AddListener((sender) =>
			{
				//Debug.Log("listened event fired " + listenedEvent.EventType);
				OnEventFired.Invoke();
			});
		}
	}
}