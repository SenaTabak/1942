﻿using UnityEngine.Events;
using UnityEngine;

namespace SGEventSystem
{
	public enum SGEventType
	{
		SpaceshipDead,
		PlayerDead
	}

	[System.Serializable]
	public class SGEvent : UnityEvent<GameObject>
	{
		public SGEventType EventType;

		public SGEvent()
		{
		}

		public SGEvent(SGEventType eventType)
		{
			EventType = eventType;
			SGEventManager.Instance.AddEvent(this);
			//Debug.Log("sg event " + EventType);
		}
	}
}