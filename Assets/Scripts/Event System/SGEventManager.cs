﻿using UnityEngine;
using System.Collections.Generic;


namespace SGEventSystem
{
	public class SGEventManager
	{
		private static SGEventManager _instance;
		public static SGEventManager Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new SGEventManager();
				}
				return _instance;
			}
		}

		private List<SGEvent> _eventList = new List<SGEvent>();

		public void AddEvent(SGEvent sgEvent)
		{
			_eventList.Add(sgEvent);
		}

		public SGEvent GetEventByType(SGEventType type)
		{
			foreach (var e in _eventList)
			{
				if (e.EventType == type)
				{
					return e;
				}
			}
			Debug.Assert(false, "bad event type(" + type + ")");
			return null;
		}
	}
}