﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class InputAction : MonoBehaviour
{
	public string ButtonId;
	public UnityEvent FirstAction;
	public UnityEvent SecondAction;

	private bool _isPressed;
	void Update()
	{
		if (Input.GetButtonUp(ButtonId))
		{
			if (_isPressed)
			{
				_isPressed = false;
				SecondAction.Invoke();
			}
			else
			{
				FirstAction.Invoke();
				_isPressed = true;
			}
		}
	}
}
