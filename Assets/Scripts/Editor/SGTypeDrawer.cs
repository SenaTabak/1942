﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;

/// <summary>
/// Custom drawer for SGType.
/// Resolves all SGType available in AssetDB, and displays them in a nice combobox popup.
/// </summary>
[CustomPropertyDrawer(typeof(SGType))]
public class SGTypeDrawer : PropertyDrawer
{
	private AssetDB AssetDB;

	public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
	{
		if (AssetDB == null)
		{
			var a = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/AssetDB.prefab", typeof(GameObject)) as GameObject;
			AssetDB = a.GetComponent<AssetDB>();
		}
		rect = EditorGUI.PrefixLabel(rect, label);
		List<string> types = new List<string>();
		foreach (var item in AssetDB.Types)
		{
			if (!types.Contains(item.Type))
			{
				types.Add(item.Type);
			}
		}

		var typeProp = property.FindPropertyRelative("Type");
		var index = EditorGUI.Popup(rect, types.IndexOf(typeProp.stringValue), types.ToArray());
		if (index >= 0)
		{
			typeProp.stringValue = types[index];
		}
	}
}
