﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;

[CustomEditor(typeof(AssetDB))]
public class AssetDBCustomInspector : Editor
{
	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		AssetDB assetDB = (AssetDB)target;

		showList = EditorGUILayout.Foldout(showList, "Types");
		if (showList)
		{
			DrawList(serializedObject.FindProperty("Types"), typeof(SGType), "Type");
		}
		var player = assetDB.PlayerSpaceship;
		EditorGUILayout.ObjectField("Player", player, typeof(SGObject), false);

		EditorGUILayout.LabelField("Spaceships");
		DrawObjectList(serializedObject.FindProperty("Spaceships"), typeof(SGObject), "Type");
		EditorGUILayout.LabelField("Guns");
		DrawObjectList(serializedObject.FindProperty("Guns"), typeof(Gun), "Type");
		EditorGUILayout.LabelField("PlayerGuns");
		DrawObjectList(serializedObject.FindProperty("PlayerGuns"), typeof(Gun), "Type");
		EditorGUILayout.LabelField("Bullets");
		DrawObjectList(serializedObject.FindProperty("Bullets"), typeof(SGObject), "Type");
		EditorGUILayout.LabelField("DropItems");
		DrawObjectList(serializedObject.FindProperty("DropItems"), typeof(SGObject), "Type");

		serializedObject.ApplyModifiedProperties();
	}
	bool showList;

	private void DrawList(SerializedProperty list, Type type, string typeFieldName)
	{
		EditorGUI.indentLevel += 1;
		//if (spaceships != null)
		{
			EditorGUILayout.BeginVertical();
			var i = 0;
			list.arraySize = EditorGUILayout.IntField("Size", list.arraySize);
			while (i < list.arraySize)
			{
				var property = list.GetArrayElementAtIndex(i);
				EditorGUILayout.BeginHorizontal();
				SerializedProperty childProp = property.FindPropertyRelative(typeFieldName);
				EditorGUILayout.LabelField(childProp.stringValue, GUILayout.Width(100));
				childProp.stringValue = EditorGUILayout.TextField(childProp.stringValue);
				if (GUILayout.Button(new GUIContent("-", "Delete"), GUILayout.Width(30)))
				{
					list.DeleteArrayElementAtIndex(i);
				}
				if (GUILayout.Button(new GUIContent("+", "Add"), GUILayout.Width(30)))
				{
					list.InsertArrayElementAtIndex(i);
				}
				EditorGUILayout.EndHorizontal();
				i++;
			}
			EditorGUILayout.EndVertical();
		}
		EditorGUI.indentLevel -= 1;
	}

	private void DrawObjectList(SerializedProperty list, Type type, string typeFieldName)
	{
		EditorGUI.indentLevel += 1;
		//if (spaceships != null)
		{
			EditorGUILayout.BeginVertical();
			var i = 0;
			list.arraySize = EditorGUILayout.IntField("Size", list.arraySize);
			while (i < list.arraySize)
			{
				var property = list.GetArrayElementAtIndex(i);
				EditorGUILayout.BeginHorizontal();
				if (property.objectReferenceValue != null)
				{
					SerializedObject propObj = new SerializedObject(property.objectReferenceValue);
					SerializedProperty childProp = propObj.FindProperty(typeFieldName).FindPropertyRelative("Type");
					EditorGUILayout.LabelField(childProp.stringValue, GUILayout.Width(100));
				}
				property.objectReferenceValue = EditorGUILayout.ObjectField(property.objectReferenceValue, type, false);
				if (GUILayout.Button(new GUIContent("-", "Delete"), GUILayout.Width(30)))
				{
					list.DeleteArrayElementAtIndex(i);
				}
				if (GUILayout.Button(new GUIContent("+", "Add"), GUILayout.Width(30)))
				{
					list.InsertArrayElementAtIndex(i);
				}
				EditorGUILayout.EndHorizontal();
				i++;
			}
			EditorGUILayout.EndVertical();
		}
		EditorGUI.indentLevel -= 1;
	}
}
