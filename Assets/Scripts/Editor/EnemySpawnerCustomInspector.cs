﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;

[CustomEditor(typeof(EnemySpawner))]
public class EnemySpawnerCustomInspector : Editor
{
	public AssetDB AssetDB;
	public override void OnInspectorGUI()
	{
		if (AssetDB == null)
		{
			var a = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/AssetDB.prefab", typeof(GameObject)) as GameObject;
			AssetDB = a.GetComponent<AssetDB>();
		}

		List<string> shipTypes = new List<string>();
		shipTypes.Add("-");
		foreach (var item in AssetDB.Spaceships)
		{
			if (!shipTypes.Contains(item.Type.Type))
			{
				shipTypes.Add(item.Type.Type);
			}
		}

		serializedObject.Update();
		var rowCountProp = serializedObject.FindProperty("RowCount");
		EditorGUILayout.PropertyField(rowCountProp);
		var rowCountVal = rowCountProp.intValue;
		var level = serializedObject.FindProperty("level");
		EditorGUILayout.BeginVertical();

		var waves = level.FindPropertyRelative("waves");
		for (int i = 0; i < waves.arraySize; i++)
		{
			EditorGUILayout.BeginHorizontal();
			var wave = waves.GetArrayElementAtIndex(i);
			var row = wave.FindPropertyRelative("row");
			row.arraySize = rowCountVal;

			for (int j = 0; j < rowCountVal; j++)
			{
				if(row.GetArrayElementAtIndex(j).stringValue == "")
				{
					row.GetArrayElementAtIndex(j).stringValue = "-";
				}
				var current = shipTypes.IndexOf(row.GetArrayElementAtIndex(j).stringValue);
				var selected = EditorGUILayout.Popup(current, shipTypes.ToArray());
				row.GetArrayElementAtIndex(j).stringValue = shipTypes[selected];
			}
			EditorGUILayout.EndHorizontal();
		}
		EditorGUILayout.EndVertical();

		if (GUILayout.Button("Add Wave"))
		{
			waves.InsertArrayElementAtIndex(waves.arraySize);
		}
		serializedObject.ApplyModifiedProperties();
	}

}
